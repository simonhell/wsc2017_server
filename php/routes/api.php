<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1/')->group(function () {

    Route::post('auth/login', 'LoginController@login');
    Route::get('auth/logout', 'LoginController@logout');
    Route::resource('place', 'PlaceController')->except(['edit', 'update']);
    Route::put('place/{place}', 'PlaceController@update');
    Route::post('place/{place}', 'PlaceController@update');
    Route::post('schedule', 'ScheduleController@store');
    Route::delete('schedule/{id}', 'ScheduleController@destroy');
    Route::get('route/search/{from}/{to}/{time?}', 'RouteController@search');
    Route::post('route/selection', 'RouteController@storeSelection');
    Route::get('lines', 'RouteController@allLines');
});

