<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoredRouteSchedule extends Model
{
    protected $hidden = [];
    protected $guarded = [];
    public $timestamps = false;
}
