<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $hidden = [];
    protected $guarded = [];
    public $timestamps = false;
    public function from() {
        return $this->belongsTo('App\Place', 'from_place_id','id');
    }

    public function to() {
        return $this->belongsTo('App\Place', 'from_place_id','id');
    }
}
