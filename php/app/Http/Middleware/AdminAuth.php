<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->get('token');
        if ($token !== null) {
            $user = User::where('token', $token)->first();
            if ($user !== null && $user->role === 'ADMIN') {
                return $next($request);
            }
        }
        return response()->json(['Unauthorized user'], 401);
    }
}
