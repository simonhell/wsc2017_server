<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->get('token');

        if ($token !== null) {
            $user = User::where('token', $token)->first();
            if ($user !== null) {
                return $next($request);
            }
        }
        return response()->json(['message'=> 'Unauthorized user'], 401);

    }
}
