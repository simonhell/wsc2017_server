<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class LoginController extends Controller
{


    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        $this->middleware('authUser', ['only' => ['logout']]);
    }

    public function login(Request $request) {
        $username = $request->get('username');
        $password = $request->get('password');
        $user = User::where('username', $username)->first();

        if($user !== null) {
            if(Hash::check($password,$user->password )) {
                $user->token = md5($username);
                $user->save();
                return response()->json(['token'=> $user->token, 'role'=>$user->role]);
            } else {
                return response()->json(['message'=>'invalid login'],401);
            }
        } else {
            return response()->json(['message'=>'invalid login'],401);
        }
    }

    public function logout(Request $request) {
        $token = $request->get('token');
        $user = User::where('token', $token)->first();
        if($user !== null) {
            $user->token = null;
            $user->save();
            return response()->json(['message' => 'logout success']);
        } else {
            return response()->json(['Unauthorized user'], 401);
        }
    }
}
