<?php

namespace App\Http\Controllers;

use App\Schedule;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    const REQUIRED = 'required';

    /**
     * ScheduleController constructor.
     */
    public function __construct()
    {
        $this->middleware('authAdmin');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate(['type' => self::REQUIRED, 'line' => self::REQUIRED, 'from_place_id' => self::REQUIRED, 'to_place_id' => self::REQUIRED, 'departure_time' => self::REQUIRED, 'arrival_time' => self::REQUIRED, 'distance' => self::REQUIRED, 'speed' => self::REQUIRED, 'status' => self::REQUIRED]);
            $schedule = new Schedule($request->except(['token']));
            $schedule->save();

            return response()->json(['message' => 'create success'], 200);

        } catch (\Exception $exception) {
            return response()->json(['message' => 'Data cannot be processed'], 422);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $schedule = Schedule::findOrFail($id);
            $schedule->delete();
            return response()->json(['message' => 'delete success'], 200);
        } catch (\Exception  $e) {
            return response()->json(['message'=> 'Data cannot be deleted'],400);
        }
    }
}
