<?php

namespace App\Http\Controllers;

use App\Place;
use App\StoredRoute;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class PlaceController extends Controller
{
    const REQUIRED = 'required';

    /**
     * PlaceController constructor.
     */
    public function __construct()
    {
        $this->middleware('authAdmin', ['only' => ['store', 'update', 'destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $places = Place::all();
        $places_response = $places->toArray();
        foreach ($places as $key => $place) {
            $places_response[$key]['frequency'] =  $this->getFrequencyForPlaceForUser($request, $place);
        }
        return response()->json($places_response);
    }

    private function getFrequencyForPlaceForUser($request, $place) {
        $token = $request->get('token');
        $user = User::where('token', $token)->first();
        if($user !== null) {
            $stored_routes = StoredRoute::where([['from_place_id', $place->id],['user_id', $user->id]] )->orWhere(([['to_place_id', $place->id],['user_id', $user->id]] ))->count();
            return $stored_routes;
        } else {
            return 0;
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate(['name' => self::REQUIRED, 'latitude' => self::REQUIRED, 'longitude' => self::REQUIRED, 'image' => self::REQUIRED]);
            $place = new Place($request->except('token', 'image'));
            $file = $request->file('image');
            $poi_factory = new PoiFactory();
            $resultXY = $poi_factory->calculate($place);
            $place->x = $resultXY['x'];
            $place->y = $resultXY['y'];
            $place->save();
            $path = $place->id . '_' . $place->name . '.' . $file->extension();
            $file->storePubliclyAs('public', $path);
            $place->update(['image_path' => $path]);
            return response()->json(['message' => 'create success']);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Data cannot be processed'], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Place $place
     * @return \Illuminate\Http\Response
     */
    public function show(Place $place)
    {
        if ($place !== null) {
            return response()->json($place);
        } else {
            return response()->json(['message' => 'Not found'], 404);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Place $place
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            $place = Place::findOrFail($id);

            if (!empty($place)) {
                $file = $request->file('image');
                if (!empty($file)) {
                    $path = $place->id . '_' . $place->name . '.' . $file->extension();
                    $file->storePubliclyAs('public', $path);
                    Storage::delete('public/' . $place->image_path);
                    $place->fill(['image_path' => $path]);
                }

                $place->fill($request->except('token', 'image'));
                $poi_factory = new PoiFactory();
                $resultXY = $poi_factory->calculate($place);
                $place->x = $resultXY['x'];
                $place->y = $resultXY['y'];
                $place->save();
            }

            return response()->json(['message' => 'update success']);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Data cannot be processed'], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $place = Place::findOrFail($id);
            if (!empty($place)) {
                    Storage::delete('public/' . $place->image_path);
                    $place->delete();
                    return response()->json(['message' => 'Delete success'], 200);
            } else {
                return response()->json(['message' => 'Data cannot be deleted'], 400);
            }
        }catch (\Exception $exception) {
            return response()->json(['message' => 'Data cannot be deleted'], 400);
        }
    }
}

class Poi
{
    private $latitude;

    private $longitude;

    public function __construct($latitude, $longitude)
    {
        $this->latitude = deg2rad($latitude);
        $this->longitude = deg2rad($longitude);
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function distanceTo(Poi $other)
    {
        $earthRadius = 6371000;

        $diffLatitude = $other->getLatitude() - $this->latitude;
        $diffLongitude = $other->getLongitude() - $this->longitude;

        $a = sin($diffLatitude / 2) * sin($diffLatitude / 2) +
            cos($other->getLatitude()) * cos($this->latitude) *
            sin($diffLongitude / 2) * sin($diffLongitude / 2);
        $c = 2 * asin(sqrt($a));

        return $c * $earthRadius;
    }
}

class PoiFactory
{
    private $start;

    private $end;

    private $startPoint;

    private $width;

    private $height;

    public function __construct($start = ['latitude' => '24.59895', 'longitude' => '54.2522'], $end = ['latitude' => '24.35429', 'longitude' => 54.66419], $width = 1280, $height = 800)
    {
        $this->start = $start;
        $this->end = $end;
        $this->width = $width;
        $this->height = $height;
        $this->mapStartPoint();
    }

    private function calc($source, $target)
    {
        $_a = new Poi($source['latitude'], $target['longitude']);
        $a = new Poi($target['latitude'], $target['longitude']);

        $y = $_a->distanceTo($a);

        $_b = new Poi($source['latitude'], $source['longitude']);
        $b = new Poi($source['latitude'], $target['longitude']);

        $x = $_b->distanceTo($b);

        return [
            'x' => $x,
            'y' => $y
        ];
    }

    private function mapStartPoint()
    {
        $calc = $this->calc($this->start, $this->end);

        $this->startPoint = [
            'x' => $calc['x'] / $this->width,
            'y' => $calc['y'] / $this->height
        ];
    }

    public function calculate($target)
    {
        $calc = $this->calc($this->start, $target);

        return [
            'x' => floor($calc['x'] / $this->startPoint['x']),
            'y' => floor($calc['y'] / $this->startPoint['y'])
        ];
    }
}
