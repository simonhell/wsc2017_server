<?php

namespace App\Http\Controllers;

use App\Place;
use App\Schedule;
use App\StoredRoute;
use App\StoredRouteSchedule;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RouteController extends Controller
{
    const TOKEN = 'token';


    /**
     * RouteController constructor.
     */
    public function __construct()
    {
        $this->middleware('authUser', ['only' => ['storeSelection']]);
    }


    public function search(Place $from, Place $to, string $time = null)
    {
        if ($time === null) {
            $time = date_format(date_create(), "H:i");
        }
        $all_places = Place::all();
        $marked_places = [];
        $current_placeId = $from->id;
        $best_routes_to_places = [];
        //Build Places Queue
        foreach ($all_places as $place) {
            $best_routes_to_places[$place->id] = [];
        }

        //Find quickest 5 routes to all places
        while ($current_placeId !== null) {
            $best_routes_to_current_place = $best_routes_to_places[$current_placeId];
            $earliest_arrival_time = $time;
            $fastest_route_to_current_place = [];

            //Find earliest arrival time for current place
            if (count($best_routes_to_current_place) > 0) {
                $fastest_route_to_current_place = $best_routes_to_current_place[0];
                $last_schedule_of_fastest_route = $fastest_route_to_current_place[count($fastest_route_to_current_place) - 1];
                $earliest_arrival_time = $last_schedule_of_fastest_route->arrival_time;
            }
            //Iterate through all schedules going away from current place and after earliest arrival time
            $schedules = Schedule::where([["from_place_id", "=", $current_placeId], ["departure_time", ">", $earliest_arrival_time]])->orderBy('arrival_time', 'desc')->get();
            foreach ($schedules as $schedule) {
                if ($schedule->departure_time >= $earliest_arrival_time) {
                    //check if the arrival place of the schedule is not already in the schedule
                    $schedules_with_same_place_id = array_filter($fastest_route_to_current_place, function ($s) use ($schedule) {
                        return $s->from_place_id !== $schedule->to_place_id;
                    });
                    if (count($schedules_with_same_place_id) >= 0) {
                        array_push($best_routes_to_places[$schedule->to_place_id],
                            array_merge($fastest_route_to_current_place, [$schedule]));
                    }
                }
            }
            $next_point_id = null;
            $next_point_earliest_arrival_time = null;

            //Sort all possible routes for all places
            foreach ($best_routes_to_places as $placeId => $best_routes_to_place) {
                if (count($best_routes_to_place) > 0) {
                    usort($best_routes_to_place, function ($a, $b) {
                        $last_schedule_a = $a[count($a) - 1];
                        $last_schedule_b = $b[count($b) - 1];

                        if ($last_schedule_a->arrival_time === $last_schedule_b->arrival_time) {
                            return 0;
                        } else if ($last_schedule_a->arrival_time < $last_schedule_b->arrival_time) {
                            return -1;
                        } else {
                            return 1;
                        }
                    });
                    $best_routes_to_places[$placeId] = array_slice($best_routes_to_place, 0, 5);
                    $best_route_to_place = $best_routes_to_place[0];
                    $last_schedule_of_fastest_route_of_place = end($best_route_to_place);

                    //Calculate next point by smallest arrival_time
                    if ($placeId !== $current_placeId && !in_array($placeId, $marked_places) && ($next_point_earliest_arrival_time === null || $next_point_earliest_arrival_time > $last_schedule_of_fastest_route_of_place->arrival_time)) {
                        $next_point_id = $placeId;
                    }
                }

            }

            array_push($marked_places, $current_placeId);
            $current_placeId = $next_point_id;
        }

        //calculate history selections

        $stored_routes = StoredRoute::where([["from_place_id", $from->id], ["to_place_id", $to->id]])->get();

        $bestRoutesReturn = array_map(function ($best_routes) {
            return ["historySelections" => 0, "schedules" => $best_routes];
        }, $best_routes_to_places[$to->id]);

        foreach ($stored_routes as $stored_route) {
            $stored_route_schedules = array_map(function ($schedule) {
                return $schedule["schedule_id"];
            }, $stored_route->schedules->toArray());

            foreach ($bestRoutesReturn as $key => $bestRoute) {
                $best_route_schedules = array_map(function ($schedule) {
                    return $schedule->id;
                }, $bestRoute["schedules"]);
                if ($best_route_schedules === $stored_route_schedules) {
                    $bestRoutesReturn[$key]["historySelections"]++;
                }
            }
        }
        return response()->json($bestRoutesReturn);
    }

    public function storeSelection(Request $request)
    {
        try {
            $stored_route = new StoredRoute($request->except('schedules', 'schedule_id', self::TOKEN));
            $stored_route->user_id = User::where(self::TOKEN, $request->get(self::TOKEN))->first()->id;
            $schedule_id = $request->get('schedule_id');
            $stored_route->save();

            if (!empty($schedule_id)) {
                $storedRouteSchedule = new StoredRouteSchedule();
                $storedRouteSchedule->schedule_id = $schedule_id;
                $storedRouteSchedule->stored_route_id = $stored_route->id;
                $storedRouteSchedule->save();
            } else {
                foreach ($request->get('schedules') as $schedule_id) {
                    $storedRouteSchedule = new StoredRouteSchedule();
                    $storedRouteSchedule->schedule_id = $schedule_id;
                    $storedRouteSchedule->stored_route_id = $stored_route->id;
                    $storedRouteSchedule->save();
                }
            }
            return response()->json(["message" => "Create success"], 200);
        } catch (\Exception $e) {
            return response()->json(["message" => 'Could not be processed'], 400);
        }
    }

    public function allLines()
    {
        $lines = DB::table("schedules")->groupBy('line')->get();
        return response()->json($lines);
    }

}

class ScheduleArray
{
    public $id;
    public $arrival_time;
    public $from_place_id;
    public $to_place_id;
    public $departure_time;

    /**
     * ScheduleArray constructor.
     * @param $id
     * @param $arrivalTime
     */
    public function __construct($id, $arrivalTime, $departure_time, $from_place_id, $to_place_id)
    {
        $this->id = $id;
        $this->from_place_id = $from_place_id;
        $this->to_place_id = $to_place_id;
        $this->arrival_time = $arrivalTime;
        $this->departure_time = $departure_time;
    }


}
