<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Place extends Model
{
    protected $hidden = [];
    protected $guarded = [];
    public $timestamps = false;


}
