<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoredRoute extends Model
{
    protected $hidden = [];
    protected $guarded = [];
    public $timestamps = false;

    public function schedules() {
        return $this->hasMany('App\StoredRouteSchedule','stored_route_id', 'id');
    }
}
