import axios from 'axios';

const instance = axios.create({
    baseURL: process.env.NODE_ENV === 'DEVELOPMENT' ? 'http://localhost:8000/' : 'http://localhost:8000/',
});

export default instance;