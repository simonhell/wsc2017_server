import vuex from 'vuex';
import Vue from 'vue';
import axios from '../plugins/axios'

Vue.use(vuex);

export default new vuex.Store({
    state: () => ({
        token: localStorage.getItem('token') || null,
        places: [],
        from: null,
        to: null,
        resultList: [],
        openedPlaceId: null,
        selectedResultId: null,
        lines: [],
        role: localStorage.getItem('role') || null,
        loginDialogIsOpened: false,
        username: localStorage.getItem('username') || null,
        loginError: null,
        storedRoutes: []
    }),
    mutations: {
        setOpenedPlace(state, placeId) {
            state.openedPlaceId = placeId;
        },
        setPlaces(state, places) {
            state.places = places;
        },
        setFrom(state, from) {
            state.from = from;
        },
        setTo(state, to) {
            state.to = to;
        },
        setResultList(state, list) {
            state.resultList = list;
        },
        setSelectedResultId(state, resultId) {
            state.selectedResultId = resultId;
        },
        setLines(state, lines) {
            state.lines = lines;
        },
        setToken(state, token) {
            state.token = token;
        },
        setRole(state, role) {
            state.role = role;
        },
        setLoginDialogIsOpened(state, login) {
            state.loginDialogIsOpened = login;
        },
        setUsername(state, username) {
            state.username = username;
        },
        setLoginError(state, loginError) {
            state.loginError = loginError;
        }
    },
    actions: {
        async login({state, commit}, loginData) {
            try {
                commit('setLoginError', null);
                const loginResponse = await axios.post('/api/v1/auth/login', loginData);
                commit('setToken', loginResponse.data.token);
                commit('setRole', loginResponse.data.role);
                commit('setUsername', loginData.username);
                commit('setLoginDialogIsOpened', false);
                localStorage.setItem('token', state.token);
                localStorage.setItem('role', state.role);
                localStorage.setItem('username', state.username);
            } catch (e) {
                commit('setLoginError', 'Invalid login');
            }

        },
        async fetchAllPlaces({getters, commit}) {
            const places = await axios.get('/api/v1/place' + getters.getTokenUrl);
            commit('setPlaces', places.data);
        },

        async fetchAllLines({getters, commit}) {
            const lines = await axios.get('/api/v1/lines' + getters.getTokenUrl);
            commit('setLines', lines.data);
        },
        async searchRoutes({state, commit, getters}, time) {
            commit('setSelectedResultId', null);
            const routes = await axios.get(`/api/v1/route/search/${state.from.id}/${state.to.id}/${time}` + getters.getTokenUrl);
            commit('setResultList', routes.data);
        },
        async saveRouteSelection({state, getters}) {
            if(state.token) {
                const selectedResult = state.resultList[state.selectedResultId];
                const body = {};
                body.from_place_id = selectedResult.schedules.slice().shift().from_place_id;
                body.to_place_id = selectedResult.schedules.slice().pop().to_place_id;
                body.schedules = selectedResult.schedules.slice().map((schedule) => schedule.id);
                await axios.post(`/api/v1/route/selection` + getters.getTokenUrl, body);
            }
        },
        async logout({getters, commit}) {
            const logoutResponse = await axios.get('/api/v1/auth/logout' + getters.getTokenUrl);
            localStorage.removeItem('token');
            localStorage.removeItem('username');
            localStorage.removeItem('role');
            commit('setToken', null);
            commit('setRole', null);
            commit('setUsername', null);
        }
    },
    getters: {
        getTokenUrl(state) {
            return "?token=" + state.token;
        },
        getLineStyles(state) {
            const colors = [
                '#F00',
                '#00F',
                '#0F0',
                '#F0F',
                '#FF0',
                '#0FF',
                '#505',
                '#005',
                '#500',
                '#550',
                '#A5A',
                '#531',
                '#888',
            ];

            return state.lines.map((line) => {
                const style = {};

                switch (line.type) {
                    case "BUS":
                        style['stroke-dashoffset'] = "5px";
                        style['stroke-dasharray'] = "5px";
                        break;
                    case "TRAIN":
                        break;
                }
                style['stroke'] = colors[line.line - 1];
                style.line = line.line;
                return style;
            });
        }
    }
})
;