import App from './App.vue'
import vuex from './store';
import axios from './plugins/axios'
import Vue from 'vue'
import VueRouter from 'vue-router'
import MapView from "./components/MapView";
import PlacesView from "./components/PlacesView";
Vue.use(VueRouter)
Vue.config.productionTip = false

const routes = [
  { path: '/', component: MapView },
  { path: '/places', component: PlacesView }
]

const router = new VueRouter({
  routes // short for `routes: routes`
})

new Vue({
  store: vuex,
  axios: axios,
  router,
  render: function (h) { return h(App) },
}).$mount('#app')
